package erreurs

sealed abstract class CalculatriceErreurs
object CalculatriceErreurs {
  final case class FormatNonValide(message: String) extends CalculatriceErreurs
  final case class OperationNonPriseEnCharge(message: String) extends CalculatriceErreurs
  final case class AucuneEntree() extends CalculatriceErreurs
}
