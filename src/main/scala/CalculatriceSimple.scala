import cats.effect.{IO, IOApp}
import cats.Monoid
import cats.implicits._

import scala.io.StdIn.readLine
import scala.annotation.tailrec
import erreurs.CalculatriceErreurs
import erreurs.CalculatriceErreurs.{AucuneEntree, FormatNonValide, OperationNonPriseEnCharge}

/** un programme simple : c'est une calculatrice qui permet de faire une addition ou une multiplication.
  *
  * (je vous l'accorde, c'est pas la calculatrice dernière génération)
  *
  * L'objectif est de concevoir ce programme en respectant au maximum la notion de pureté.
  *
  * Pour ce faire on utilisera cats IO pour la gestion des erreurs et le dialogue entre l'utilisateur.
  *
  * On utilisera les monoïdes de cats pour les opérations d'addition et multiplication
  *
  * On utilisera cats IOApp pour jouer l'application.
  *
  * On utilisera un ADT pour la gestion des erreurs.
  */
object CalculatriceSimple extends IOApp.Simple {
  val FloatAdditionMonoid: Monoid[Float] = new Monoid[Float] {
    def empty: Float = 0
    def combine(x: Float, y: Float): Float = x + y
  }
  val floatMultiplicationMonoid: Monoid[Float] = new Monoid[Float] {
    def empty: Float = 1
    def combine(x: Float, y: Float): Float = x * y
  }

  /** prend en paramètre la saisie utilisateur et donne le tye [[SaisieUtilisateurDecoupee]] qui sépare l'opération des
    * nombres toujours en string.
    *
    * Pour le deuixème nombre, on enlevera les espaces avec trim.
    *
    * @param saisieUtilisateur
    *   la saisie utilisateur
    * @return
    *   un Either contenant soit la saisie utilisateur découpée en trois partie et contenu dans le type
    *   [[SaisieUtilisateurDecoupee]] soit une erreur
    */
  def decouperSaisie(saisieUtilisateur: String): Either[CalculatriceErreurs, SaisieUtilisateurDecoupee] = {
    @tailrec
    def decouperSaisieTailRec(
        saisie: List[Char],
        nombre1: String
    ): Either[CalculatriceErreurs, SaisieUtilisateurDecoupee] =
      saisie match {
        // cas d'arrêt
        case Nil          => Left(AucuneEntree())
        case '+' :: queue => verifierFormat(nombre1, queue, '+', saisieUtilisateur)
        case '*' :: queue => verifierFormat(nombre1, queue, '*', saisieUtilisateur)
        case '-' :: _     => Left(OperationNonPriseEnCharge("-"))
        case '/' :: _     => Left(OperationNonPriseEnCharge("/"))
        // recursions
        case ' ' :: queue  => decouperSaisieTailRec(queue, nombre1)
        case tete :: queue =>
          // todo: je n'aime pas cette manière, à chaque recursion je cast le char en string puis je fais un concate
          // todo: existe-t-il un moyen plus optimisé et plus "élégant" ?
          decouperSaisieTailRec(queue, nombre1 ++ tete.toString)
      }
    decouperSaisieTailRec(saisieUtilisateur.toList, "")
  }

  /** Prend en paramètre les deux nombres à vérifier, l'opérateur et la saisie non traitée et vérifie si les nombres
    * sont bien écrits. Par exemple on regarde si l'utilisateur a bien écrit les nombres avec un "." et non pas avec une
    * ",". On s'assure de nettoyer les espaces en trop pour le premier nombre.
    */
  def verifierFormat(
      nombre1Averifier: String,
      nombre2Averifier: List[Char],
      operateur: Char,
      saisieUtilisateur: String
  ): Either[CalculatriceErreurs, SaisieUtilisateurDecoupee] = {
    val nombre1 = nombre1Averifier.replaceAll(" ", "")
    val nombre2 = nombre2Averifier.mkString.replaceAll(" ", "")

    if (verifierLesDeuxNombres(nombre1, nombre2)) {
      Right(SaisieUtilisateurDecoupee(nombre1, operateur, nombre2))
    } else {
      Left(FormatNonValide(saisieUtilisateur))
    }
  }
  // todo:je pense que le plus sûre serait de vérifier si on a bien des nombres là je peux oublier des cas
  def verifierLesDeuxNombres(nombreGauche: String, nombreDroit: String): Boolean = {
    val symbolesInterdis = "[a-zA-Z,;:?&é]".r

    (symbolesInterdis.findFirstIn(nombreGauche), symbolesInterdis.findFirstIn(nombreDroit)) match {
      case (None, None) => true
      case _            => false
    }
  }

  /** Prend en paramètre la saisie de l'utilisateur, fait une opération de nettoyage et d'analyse, et renvoie le calcul
    * @param saisieUtilisateur
    *   la saisie de l'utilisateur
    * @return
    *   retourne le résultat ou une erreur
    */
  def calculerNombre(saisieUtilisateur: String): Either[CalculatriceErreurs, Float] =
    decouperSaisie(saisieUtilisateur) match {
      case Right(saisie: SaisieUtilisateurDecoupee) => detecterOperation(saisie)
      case Left(erreurs: CalculatriceErreurs)       => Left(erreurs)
    }

  /** détecte l'opération et fait appel au bon monoïde
    * @param saisieDecoupee
    *   la saisie utilisateur
    * @return
    *   retourne le résultat de l'opération ou bien
    */
  // pas tous les cas sont représentés, mais on fait appel à cette méthode uniquement quand on est sûr d'avoir
  // les deux bonnes opérations.
  // todo : le compilateur soulève un warning, il faut traiter !
  def detecterOperation(saisieDecoupee: SaisieUtilisateurDecoupee): Either[CalculatriceErreurs, Float] =
    saisieDecoupee.operateur match {
      case Some('+') => Right(FloatAdditionMonoid.combine(saisieDecoupee.nombre1, saisieDecoupee.nombre2))
      case Some('*') => Right(floatMultiplicationMonoid.combine(saisieDecoupee.nombre1, saisieDecoupee.nombre2))
    }

  val run: IO[Unit] =
    IO {
      println("Entrez un calcul (addition et multiplication possibles).")
      val saisie = readLine()
      val calcul = calculerNombre(saisie)
      calcul match {
        case Right(resultat)                          => println(resultat)
        case Left(FormatNonValide(message))           => println(s"Ecrire $message de cette façon n'est pas accepté.")
        case Left(OperationNonPriseEnCharge(message)) => println(s"L'opération $message n'est pas prise en charge.")
        case Left(AucuneEntree())                     => println(s"Vous n'avez rien saisie.")
      }
    }.foreverM

}
