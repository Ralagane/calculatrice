import cats.implicits.catsSyntaxOptionId

case class SaisieUtilisateurDecoupee(nombre1: Float, operateur: Option[Char], nombre2: Float) {}

object SaisieUtilisateurDecoupee {
  def apply(nombre1: String, operateur: Char, nombre2: String): SaisieUtilisateurDecoupee =
    SaisieUtilisateurDecoupee(nombre1.toFloat, operateur.some, nombre2.toFloat)
}
