# Calculatrice

Une (très) simple calculatrice faite en scala type level.

## Le but

Un programme simple : c'est une calculatrice qui permet de faire une addition ou une multiplication.
(je vous l'accorde, c'est pas la calculatrice dernière génération).

Le deuxième but est d'apprendre à mieux utiliser la bibliothèque cats et surtout améliorer le lisibilité de mon code. 
En effet, on me reproche assez souvent que ma manière de coder est difficile à lire de part la complexité de mon code et aussi
la manière de nommer mes fonctions.

Du coup, la seule manière, je pense, pour régler ces soucis est de manipuler à travers un petit exercice. en 
me fixant un objecitf.

L'objectif est de concevoir ce programme en faisant du type level.
- [ ] Pour ce faire on utilisera cats IO pour la gestion des erreurs et le dialogue entre l'utilisateur.
- [ ] On utilisera les monoïdes de cats pour les opérations d'addition et multiplication.
- [ ] On utilisera cats IOApp pour jouer l'application.
- [ ] On utilisera un ADT pour la gestion des erreurs.


# Todo
- [ ] Améliorer les commentaires du code
- [x] Mettre une boucle infinie pour que l'utilisateur puisse calculer à la suite (garder la notion de pureté aussi)
- [ ] Si je suis motivé refaire la calculatrice SANS utiliser les opérations primitive de scala (on fait tout à la main :o)
- [ ] Améliorer le nom des méthodes, la structure du code
- [ ] Implémenter des tests
- [ ] Implémenter un endpoit ! (implémenter un backend pour une utilisation en ligne de ce projet)
- [ ] structurer correcter l'architecture de l'application
- [ ] mettre le code en anglais
- [ ] Faire tous les todo
