name := "calculatrice"

version := "0.1"

scalaVersion := "2.12.15"

libraryDependencies += "org.typelevel" %% "cats-core" % "2.6.1"
libraryDependencies += "org.typelevel" %% "cats-effect" % "3.3.0"
